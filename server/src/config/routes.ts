import {Router, static as expressStatic} from 'express'
import {resolve} from 'path'
import cors from 'cors'
import bodyParser from 'body-parser'
import {handleError} from 'middleware/handleError'
import {getUserId} from 'components/User/middleware'

import user from 'components/User/api'
import survey from 'components/Survey/api'
import surveyResponse from 'components/SurveyResponse/api'

export const router = Router()

export const routes = {
  root: '/',
  user: {
    current: '/api/v1/users/me',
    signIn: '/api/v1/users/sign_in',
    signUp: '/api/v1/users/sign_up',
    forgotPassword: '/api/v1/users/forgot_password',
    verify: '/api/v1/users/verify',
    changePassword: '/api/v1/users/change_password',
  },
  survey: {
    current: '/api/v1/survey',
  },
  surveyResponse: {
    current: '/api/v1/survey_response',
  },
  frontend: {
    user: {
      verify: '/users/verify',
      changePassword: '/users/change_password',
    }
  }
}

router.use('*', cors())
router.use(bodyParser.json())

router.get(routes.user.current, user.current)
router.post(routes.user.signIn, user.signIn)
router.post(routes.user.signUp, user.signUp)
router.post(routes.user.forgotPassword, user.forgotPassword)
router.post(routes.user.verify, user.verify)
router.post(routes.user.changePassword, user.changePassword)

router.get(routes.survey.current, getUserId, survey.get)

router.get(routes.surveyResponse.current, getUserId, surveyResponse.current)
router.post(routes.surveyResponse.current, getUserId, surveyResponse.create)

router.use(expressStatic('../client/build'))
const frontendFile = resolve(__dirname, '..', '..', '..', 'client/build/index.html')
router.get('*', (req, res) => {
  res.sendFile(frontendFile)
})

router.use(handleError)
