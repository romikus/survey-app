import fs from 'fs'
import path from 'path'
import mailer from 'config/mailer'
import Handlebars from 'handlebars'

export const sendMail = (
  {to, subject, template, data = {}}: {to: string, subject: string, template: string, data?: object}
) => new Promise((resolve, reject) => {
  const filePath = path.resolve(process.env.NODE_PATH as string, `${template}.hbs`)
  fs.readFile(filePath, 'utf8', async (err, input) => {
    if (err)
      return reject(err)

    const view = Handlebars.compile(input)
    const html = view(data)

    await mailer.sendMail({
      from: process.env.MAIL_FROM,
      to, subject, html
    })

    resolve()
  })
})
