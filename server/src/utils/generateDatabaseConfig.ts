import fs from 'fs'
import path from 'path'
import config from '../../database'

fs.writeFileSync(path.join(__dirname, '../config/database.json'), JSON.stringify(config), 'utf8')
