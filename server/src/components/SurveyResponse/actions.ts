import SurveyResponse from 'models/SurveyResponse'

const actions = {
  current: ({userId}: {userId: number}) =>
    SurveyResponse.findOne({where: {userId}}),

  create: async ({userId, response}: {userId: number, response: string}) => {
    const survey = await actions.current({userId})
    if (survey)
      await survey.update({response})
    else
      await SurveyResponse.create({userId, response})
  }
}

export default actions
