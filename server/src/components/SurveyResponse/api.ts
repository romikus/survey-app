import {Request, Response} from 'express'
import * as Yup from 'yup'
import actions from './actions'

const schemas = {
  create: Yup.object().shape({
    response: Yup.string().required(),
  }),
}

export default {
  current: async (req: Request, res: Response) => {
    const {userId} = req
    const response = await actions.current({userId})
    res.json(response)
  },

  create: async (req: Request, res: Response) => {
    const {userId} = req
    const {response} = schemas.create.validateSync(req.body)
    await actions.create({userId, response})
    res.end()
  }
}
