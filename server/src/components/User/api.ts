import {Request, Response} from 'express'
import * as Yup from 'yup'
import actions from './actions/auth'

const schemas = {
  signIn: Yup.object().shape({
    email: Yup.string().email('Invalid email').required(),
    password: Yup.string().min(6).required(),
  }),

  signUp: Yup.object().shape({
    email: Yup.string().email('Invalid email').required(),
    password: Yup.string().min(6).required(),
  }),

  forgotPassword: Yup.object().shape({
    email: Yup.string().email('Invalid email').required(),
  }),

  verify: Yup.object().shape({
    token: Yup.string().required(),
  }),

  changePassword: Yup.object().shape({
    password: Yup.string().min(6).required(),
    token: Yup.string().required(),
  }),
}

export default {
  current: async (req: Request, res: Response) => {
    const authHeader = req.headers.authorization
    if (authHeader) {
      const user = await actions.current({authHeader})
      if (user) {
        const {id, email} = user
        return res.json({id, email})
      }
    }

    res.json(null)
  },

  signIn: async (req: Request, res: Response) => {
    const params = schemas.signIn.validateSync(req.body, {stripUnknown: true})
    const data = await actions.signIn(params)
    data ? res.json(data) : res.status(422).json({message: 'Invalid email or password', type: 'not_found'})
  },

  signUp: async (req: Request, res: Response) => {
    const params = schemas.signUp.validateSync(req.body, {stripUnknown: true})
    await actions.signUp(params)
    res.end()
  },

  forgotPassword: async (req: Request, res: Response) => {
    const {email} = schemas.forgotPassword.validateSync(req.body)
    const emailSent = await actions.forgotPassword({email})
    emailSent ? res.end() : res.status(422).json({message: 'Email is not registered', type: 'not_found'})
  },

  verify: async (req: Request, res: Response) => {
    const params = schemas.verify.validateSync(req.body, {stripUnknown: true})
    const data = await actions.verify(params)
    data ? res.send(data) : res.send(null)
  },

  changePassword: async (req: Request, res: Response) => {
    const params = schemas.changePassword.validateSync(req.body, {stripUnknown: true})
    const data = await actions.changePassword(params)
    data ? res.send(data) : res.send(null)
  },
}
