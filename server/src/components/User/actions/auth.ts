import User from 'models/User'
import bcrypt from 'bcrypt'
import {sendMail} from 'utils/sendMail'
import randomString from 'crypto-random-string'
import {routes} from 'config/routes'
import jwt from 'jsonwebtoken'

const getToken = (user: {id: number}) =>
  jwt.sign({id: user.id}, process.env.ACCESS_TOKEN_SECRET as string)

type emailAndPassword = {
  email: string,
  password: string,
}

const actions = {
  getIdFromHeader: ({authHeader}: {authHeader: string}) => {
    try {
      const accessToken = authHeader.split(' ')[1]
      const {id: stringId} = jwt.verify(accessToken, process.env.ACCESS_TOKEN_SECRET as string) as { id: string }
      const id = parseInt(stringId)
      return isNaN(id) ? undefined : id
    } catch (err) {}
  },

  current: async ({authHeader}: {authHeader: string}) => {
    const id = actions.getIdFromHeader({authHeader})
    return id && await User.findByPk(id)
  },

  signIn: async ({email, password}: emailAndPassword) => {
    const user = await User.findOne({where: {email}})
    if (user && bcrypt.compareSync(password, user.encryptedPassword))
      return {user, accessToken: getToken(user)}
  },

  signUp: async ({email, password}: emailAndPassword) => {
    const token = randomString({length: 16})
    await User.create({
      email,
      confirmationToken: token,
      encryptedPassword: bcrypt.hashSync(password, 12),
    })

    sendMail({
      to: email,
      subject: 'Confirmation instructions',
      template: 'components/User/emails/confirmationInstructions',
      data: {
        email,
        confirmationUrl: `${process.env.FRONTEND_URL}${routes.frontend.user.verify}?token=${token}`
      }
    })
  },

  verify: async ({token}: { token: string }) => {
    const user = await User.findOne({where: {confirmationToken: token}})
    if (user) {
      await user.update({confirmationToken: null, confirmedAt: Date.now()})
      return {user, accessToken: getToken(user)}
    }
  },

  forgotPassword: async ({email}: { email: string }) => {
    const user = await User.findOne({where: {email}})
    if (user) {
      const token = randomString({length: 16})
      await user.update({resetPasswordToken: token})

      sendMail({
        to: user.email,
        subject: 'Reset password instructions',
        template: 'components/User/emails/resetPasswordInstructions',
        data: {
          email: user.email,
          changePasswordUrl: `${process.env.FRONTEND_URL}${routes.frontend.user.changePassword}?token=${token}`,
        }
      })

      return true
    }

    return false
  },

  changePassword: async ({password, token}: { password: string, token: string }) => {
    const user = await User.findOne({where: {resetPasswordToken: token}})
    if (user) {
      await user.update({
        encryptedPassword: bcrypt.hashSync(password, 12),
        resetPasswordToken: null
      })
      return {user, accessToken: getToken(user)}
    }
  }
}

export default actions
