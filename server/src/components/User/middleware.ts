import {RequestHandler} from 'express'
import actions from './actions/auth'

export const getUserId: RequestHandler = async (req, res, next) => {
  const {authorization} = req.headers
  if (authorization) {
    const id = await actions.getIdFromHeader({authHeader: authorization})
    if (id) {
      req.userId = id
      return next()
    }
  }
  res.sendStatus(401)
}
