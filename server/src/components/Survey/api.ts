import {Request, Response} from 'express'
import actions from './actions'

export default {
  get: async (req: Request, res: Response) => {
    const survey = actions.get()
    res.json(survey)
  }
}
