import fs from 'fs'
import path from 'path'

const surveyJSON = fs.readFileSync(path.resolve(__dirname, '../../../survey.json')).toString()

export default {
  get: () => surveyJSON
}
