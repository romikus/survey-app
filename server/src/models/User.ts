import { Model, DataTypes } from 'sequelize'
import db from 'config/sequelize'

export default class User extends Model {
  id!: number
  email!: string
  encryptedPassword!: string
  confirmationToken!: string
  resetPasswordToken!: string
  confirmedAt!: Date
  createdAt!: Date
  updatedAt!: Date
}

User.init({
  id: {
    type: DataTypes.INTEGER.UNSIGNED,
    autoIncrement: true,
    primaryKey: true,
  },
  email: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  encryptedPassword: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  confirmationToken: {
    type: DataTypes.STRING,
  },
  resetPasswordToken: {
    type: DataTypes.STRING,
  },
  confirmedAt: {
    type: DataTypes.DATE,
  }
}, {
  tableName: 'users',
  sequelize: db,
})
