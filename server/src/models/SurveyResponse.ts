import { Model, DataTypes } from 'sequelize'
import db from 'config/sequelize'

export default class SurveyResponse extends Model {
  id!: number
  userId!: number
  response!: string
}

SurveyResponse.init({
  id: {
    type: DataTypes.INTEGER.UNSIGNED,
    autoIncrement: true,
    primaryKey: true,
  },
  userId: {
    type: DataTypes.INTEGER.UNSIGNED,
    allowNull: false,
  },
  response: {
    type: DataTypes.STRING,
    allowNull: false,
  }
}, {
  tableName: 'surveyResponses',
  sequelize: db,
})
