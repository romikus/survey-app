import {Migration} from 'rake-db'

export const change = (db: Migration, up: boolean) => {
  db.createTable('users', (t) => {
    t.string('email')
    t.string('encryptedPassword')
    t.string('confirmationToken')
    t.string('resetPasswordToken')
    t.date('confirmedAt')
    t.timestamps()
  })
}
