import bcrypt from 'bcrypt'
import User from '../../src/models/User'

export const up = async () => {
  if (await User.findOne({where: {email: 'test@user.com'}}))
    return

  await User.create({
    email: 'test@user.com',
    encryptedPassword: bcrypt.hashSync('password', 12),
  })
}

export const down = async () => {
  const user = await User.findOne({where: {email: 'test@user.com'}})
  if (user)
    await user.destroy()
}

