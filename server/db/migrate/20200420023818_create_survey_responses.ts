import {Migration} from 'rake-db'

export const change = (db: Migration, up: boolean) => {
  db.createTable('surveyResponses', (t) => {
    t.belongsTo('user')
    t.column('response', 'jsonb')
    t.timestamps()
  })
}
