#!/usr/bin/env bash

cd client && yarn install && yarn start &
cd ../server && yarn install && yarn rake-db migrate && yarn start
