# Survey App

## Requirements

node.js 10+
npm or yarn (yarn seems to be faster, but npm is default)
postgresql

Installation of node.js: https://github.com/nvm-sh/nvm#installing-and-updating

Installation of postgres to macos:

- [Install brew](https://brew.sh/) /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"
- Install postgres via brew: `brew install postgresql`

It should create postgres user with your username and blank password.

## Setup

Copy environment files:

```bash
cp client/.env.example client/.env
cp server/.env.example server/.env
```

Edit `server/.env`:

To verify emails app requires your email account, locally it can be your gmail account, fill these fields:

```.env
MAIL_SERVICE=gmail
MAIL_EMAIL=gmail_email
MAIL_PASSWORD=password
MAIL_FROM=gmail_email
```

On the server gmail will not work, but yandex works fine.

Change DEV_DATABASE_URL with your username and password, `postgres://username:@127.0.0.1:5432/survey` example with blank password.

Create db with command:

```bash
createdb survey
```

Install packages:

```bash
cd client && yarn install && cd ..
cd server && yarn install
```

Migrate db:

```bash
cd server
node src/utils/generateDatabaseConfig.js
yarn sequelize db:migrate
```

Now app can be started:

```bash
cd client && yarn start
# in new terminal tab:
cd server && yarn start
```

## survey.json

JSON from https://surveyjs.io/ located in `server/survey.json`
