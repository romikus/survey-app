export const loadStyle = (src: string) => new Promise(resolve => {
  if (document.head.querySelector(`[href="${src}"]`))
    return resolve()

  const link = document.createElement('link')
  link.rel = 'stylesheet'
  link.href = src
  link.onload = resolve
  document.head.appendChild(link)
})
