export const loadScript = (src: string) => new Promise(resolve => {
  if (document.head.querySelector(`[src="${src}"]`))
    return resolve()

  const script = document.createElement('script')
  script.src = src
  script.onload = resolve
  document.head.appendChild(script)
})
