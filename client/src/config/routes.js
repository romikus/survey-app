export const routes = {
  root: () => '/',
  user: {
    signIn: () => '/users/sign_in',
    signUp: () => '/users/sign_up',
    forgotPassword: () => '/users/forgot_password',
    verify: () => '/users/verify',
    changePassword: () => '/users/change_password',
  },
  api: {
    user: {
      current: () => '/api/v1/users/me',
      signIn: () => '/api/v1/users/sign_in',
      signUp: () => '/api/v1/users/sign_up',
      forgotPassword: () => '/api/v1/users/forgot_password',
      verify: () => '/api/v1/users/verify',
      changePassword: () => '/api/v1/users/change_password',
    },
    survey: {
      current: () => '/api/v1/survey',
    },
    surveyResponse: {
      current: () => '/api/v1/survey_response',
    }
  }
}
