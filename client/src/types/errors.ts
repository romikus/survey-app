export interface FetchError extends Error {
  type?: string
}
