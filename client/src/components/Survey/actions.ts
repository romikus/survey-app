import {get, post} from 'utils/fetch'
import {routes} from 'config/routes'

export const getSurvey = async () =>
  await get(routes.api.survey.current())

export const getSurveyResponse = async () =>
  await get(routes.api.surveyResponse.current())

export const saveResonse = async ({response}: {response: string}) =>
  await post(routes.api.surveyResponse.current(), {response})
