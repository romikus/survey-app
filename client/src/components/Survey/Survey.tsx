import React from 'react'
import {loadScript} from 'utils/loadScript'
import {loadStyle} from 'utils/loadStyle'
import {getSurvey, getSurveyResponse, saveResonse} from './actions'
import Box from '@material-ui/core/Box/Box'
import Button from '@material-ui/core/Button/Button'
import './style.css'

const scripts: {[key: string]: string} = {
  jquery: 'https://code.jquery.com/jquery-3.2.1.slim.min.js',
  survey: 'https://surveyjs.azureedge.net/1.5.19/survey.jquery.js',
  bootstrapMaterial: 'https://unpkg.com/bootstrap-material-design@0.5.10/dist/js/material.js',
  bootstrapMaterialRipples: 'https://unpkg.com/bootstrap-material-design@0.5.10/dist/js/ripples.js',
}

const styles: {[key: string]: string} = {
  bootstrap: 'https://unpkg.com/bootstrap@3.3.7/dist/css/bootstrap.min.css',
  bootstrapMaterial: 'https://unpkg.com/bootstrap-material-design@0.5.10/dist/css/bootstrap-material-design.css',
  bootstrapMaterialRipples: 'https://unpkg.com/bootstrap-material-design@0.5.10/dist/css/ripples.css',
}

declare const $: any
declare const Survey: any

export default () => {
  const surveyRef = React.useRef(null)
  const [showEditButton, setShowEditButton] = React.useState(false)
  const [surveyModel, setSurveyModel] = React.useState(undefined)

  const loadStuff = async () => {
    const scriptsPromise = new Promise(async (resolve) => {
      await loadScript(scripts.jquery)
      await loadScript(scripts.survey)
      await loadScript(scripts.bootstrapMaterial)
      await loadScript(scripts.bootstrapMaterialRipples)
      resolve()
    })

    const stylePromises = Object.keys(styles).map(key =>
      loadStyle(styles[key])
    )

    const depsPromise = Promise.all(stylePromises.concat([scriptsPromise]))
    const [survey, response] = await Promise.all([getSurvey(), getSurveyResponse(), depsPromise])

    $.material.init()

    Survey.defaultBootstrapMaterialCss.navigationButton = "btn btn-green";
    Survey.defaultBootstrapMaterialCss.rating.item = "btn btn-default my-rating";
    Survey.StylesManager.applyTheme("bootstrapmaterial")

    const model = new Survey.Model(JSON.parse(survey))
    setSurveyModel(model)

    if (response)
      model.data = response.response

    model.onComplete.add((survey: any) => {
      saveResonse({
        response: JSON.stringify(survey.data)
      })
      setShowEditButton(true)
    })

    $(surveyRef.current).Survey({model})
  }

  React.useEffect(() => {
    loadStuff()
  }, [])

  const editAnswers = () => {
    if (!surveyModel)
      return
    const model: any = surveyModel
    model.clear(false)
    model.render()
    setShowEditButton(false)
  }

  return <React.Fragment>
    <div ref={surveyRef}/>
    {showEditButton &&
      <Box m={2}>
        <Button variant='contained' onClick={editAnswers}>Edit answers</Button>
      </Box>
    }
  </React.Fragment>
}
