import React from 'react'
import {Router, Switch, Route} from 'react-router-dom'
import history from 'utils/history'
import {UserRoute, NoUserRoute} from 'components/User/Route'
import {routes} from 'config/routes'
import Survey from 'components/Survey/Survey'
import UserAuth from 'components/User/Auth/Auth'
import UserVerify from 'components/User/Auth/Verify/Verify'
import UserChangePassword from 'components/User/Auth/ChangePassword/ChangePassword'
import Layout from 'components/App/Layout/Layout'
import NotFound from 'components/App/NotFound/NotFound'

export default () =>
  <Router history={history}>
    <Layout>
      <Switch>
        <NoUserRoute path={routes.user.signIn()} exact component={UserAuth} />
        <NoUserRoute path={routes.user.signUp()} exact component={UserAuth} />
        <NoUserRoute path={routes.user.forgotPassword()} exact component={UserAuth} />
        <NoUserRoute path={routes.user.verify()} exact component={UserVerify} />
        <NoUserRoute path={routes.user.changePassword()} exact component={UserChangePassword} />
        <UserRoute path={routes.root()} exact component={Survey} />
        <Route component={NotFound} />
      </Switch>
    </Layout>
  </Router>
