import React from 'react'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Box from '@material-ui/core/Box'
import Button from '@material-ui/core/Button'
import {logOut} from 'components/User/actions'
import CurrentUser from 'components/User/Current'
import {observer} from 'mobx-react'
import './style.css'

export default observer(({children}: {children: React.ReactNode}) =>
  <React.Fragment>
    {CurrentUser.user &&
      <AppBar id='appHeader'>
        <Toolbar>
          <Box textAlign='right' flexGrow={1}>
            <Button onClick={logOut} color="inherit">Log Out</Button>
          </Box>
        </Toolbar>
      </AppBar>
    }
    {children}
  </React.Fragment>
)
