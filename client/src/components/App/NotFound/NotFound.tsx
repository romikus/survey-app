import React from 'react'
import style from './style.module.css'

export default () =>
  <div className={style.container}>
    <h1 className={style.title}>404</h1>
    <h3 className={style.subtitle}>This page could not be found.</h3>
  </div>
