import IUser from 'components/User/Type'
import CurrentUser from 'components/User/Current'
import {routes} from 'config/routes'
import {get} from 'utils/fetch'
import UserType from './Type'
import history from '../../utils/history'

export const getCurrentUser = async () => {
  if (CurrentUser.loaded)
    return

  const accessToken = localStorage.getItem('userAccessToken')
  if (!accessToken)
    return CurrentUser.setUser(undefined)

  const user = await get(routes.api.user.current()) as unknown as IUser
  CurrentUser.setUser(user)
}

export const setCurrentUser = ({user, accessToken}: {user: UserType, accessToken: string}) => {
  CurrentUser.user = user
  localStorage.setItem('userAccessToken', accessToken)
  history.push(routes.root())
}

export const logOut = () => {
  localStorage.removeItem('userAccessToken')
  CurrentUser.setUser(undefined)
}
