import React from 'react'
import TextField from '@material-ui/core/TextField/TextField'
import DialogActions from '@material-ui/core/DialogActions/DialogActions'
import Button from '@material-ui/core/Button'
import { Formik, Form } from 'formik'
import * as Yup from 'yup'
import {signUp} from './actions'
import style from './style.module.css'

const signUpSchema = Yup.object().shape({
  email: Yup.string().email('Invalid email').required(),
  password: Yup.string().min(6).required(),
})

const verificationMessage = 'You will receive an email with instructions for how to confirm your email address in a few minutes.'

export default React.memo(() => {
  const [successMessage, setSuccessMessage] = React.useState<string>()

  return <Formik
    initialValues={{
      email: '',
      password: '',
    }}
    validationSchema={signUpSchema}
    onSubmit={async (values, actions) => {
      try {
        await signUp(values)
        setSuccessMessage(verificationMessage)
      } catch (err) {
        if (err.type === 'record_not_unique')
          actions.setFieldError('email', 'Email is already taken')
        else
          throw err
      }
    }}
  >
    {({values, errors, touched, handleChange, handleBlur, isSubmitting}) => (
      <Form>
        {successMessage && <div className={style.notice}>{successMessage}</div>}
        <TextField
          fullWidth
          name='email'
          type='email'
          label='Email'
          onChange={handleChange}
          onBlur={handleBlur}
          value={values.email}
          error={!!(errors.email && touched.email)}
          helperText={touched.email && errors.email}
        />
        <TextField
          fullWidth
          name='password'
          type='password'
          label='Password'
          onChange={handleChange}
          onBlur={handleBlur}
          value={values.password}
          error={!!(errors.password && touched.password)}
          helperText={touched.password && errors.password}
        />
        <DialogActions>
          <Button disabled={isSubmitting} variant='contained' color="primary" type='submit'>
            Create Account
          </Button>
        </DialogActions>
      </Form>
    )}
  </Formik>
})
