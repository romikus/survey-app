import React from 'react'
import {Link} from 'react-router-dom'
import {RouteComponentProps} from 'react-router'
import Container from '@material-ui/core/Container'
import Paper from '@material-ui/core/Paper'
import Tabs from '@material-ui/core/Tabs'
import Tab from '@material-ui/core/Tab'
import DialogContent from '@material-ui/core/DialogContent'
import SignIn from './SignIn'
import SignUp from './SignUp'
import ForgotPassword from './ForgotPassword'
import {routes} from 'config/routes'
import style from './style.module.css'

export default ({location: {pathname}}: RouteComponentProps) => {
  const pathes = [routes.user.signIn(), routes.user.signUp(), routes.user.forgotPassword()]
  const [tab, setTab] = React.useState(pathes.indexOf(pathname))

  const changeTab = (e: React.ChangeEvent<{}>, value: number) =>
    setTab(value)

  return <div className={style.container}>
    <Container maxWidth='sm'>
      <Paper elevation={3}>
        <Tabs value={tab} onChange={changeTab} aria-label="simple tabs example">
          <Tab label='Sign In' component={Link} to={routes.user.signIn()} />
          <Tab label='Sign Up' component={Link} to={routes.user.signUp()} />
          <Tab label='Forgot password' component={Link} to={routes.user.forgotPassword()} />
        </Tabs>
        <DialogContent>
          {tab === 0 && <SignIn />}
          {tab === 1 && <SignUp />}
          {tab === 2 && <ForgotPassword />}
        </DialogContent>
      </Paper>
    </Container>
  </div>
}
