import React from 'react'
import TextField from '@material-ui/core/TextField/TextField'
import DialogActions from '@material-ui/core/DialogActions/DialogActions'
import Button from '@material-ui/core/Button'
import { Formik, Form } from 'formik'
import * as Yup from 'yup'
import {signIn} from './actions'
import style from './style.module.css'

const signInSchema = Yup.object().shape({
  email: Yup.string().email('Invalid email').required(),
  password: Yup.string().min(6).required(),
})

const notFoundMessage = 'Invalid email or password'

export default React.memo(() => {
  const [errorMessage, setErrorMessage] = React.useState<string>()

  return <Formik
    initialErrors={{
      general: errorMessage
    }}
    initialValues={{
      general: undefined,
      email: '',
      password: '',
    }}
    validationSchema={signInSchema}
    onSubmit={async (values) => {
      try {
        await signIn(values)
      } catch (err) {
        if (err.type === 'not_found')
          setErrorMessage(notFoundMessage)
        else
          throw err
      }
    }}
  >
    {({values, errors, touched, handleChange, handleBlur, isSubmitting}) => {
      return <Form onChange={() => setErrorMessage(undefined)}>
        {errorMessage && <div className={style.error}>{errorMessage}</div>}
        <TextField
          fullWidth
          name='email'
          type='email'
          label='Email'
          onChange={handleChange}
          onBlur={handleBlur}
          value={values.email}
          error={!!(errors.email && touched.email)}
          helperText={touched.email && errors.email}
        />
        <TextField
          fullWidth
          name='password'
          type='password'
          label='Password'
          onChange={handleChange}
          onBlur={handleBlur}
          value={values.password}
          error={!!(errors.password && touched.password)}
          helperText={touched.password && errors.password}
        />
        <DialogActions>
          <Button disabled={isSubmitting} variant='contained' color="primary" type='submit'>
            Submit
          </Button>
        </DialogActions>
      </Form>
    }}
  </Formik>
})
