import {post} from 'utils/fetch'
import {routes} from 'config/routes'
import {setCurrentUser} from 'components/User/actions'

export const changePassword = async ({token, password}: {token: string, password: string}) => {
  const data = await post(routes.api.user.changePassword(), {token, password})
  if (data)
    setCurrentUser(data)
}
