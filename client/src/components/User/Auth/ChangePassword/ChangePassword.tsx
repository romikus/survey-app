import React from 'react'
import Paper from '@material-ui/core/Paper'
import TextField from '@material-ui/core/TextField'
import DialogContent from '@material-ui/core/DialogContent'
import DialogActions from '@material-ui/core/DialogActions'
import Button from '@material-ui/core/Button'
import { Formik, Form } from 'formik'
import * as Yup from 'yup'
import {changePassword} from './actions'
import style from 'components/User/Auth/style.module.css'
import {RouteChildrenProps} from 'react-router'

const changePasswordSchema = Yup.object().shape({
  password: Yup.string().min(6).required(),
})

export default ({location: {search}}: RouteChildrenProps) => {
  const match = search.match(/\btoken=([^&]+)/)
  let token: string
  if (match)
    token = match[1]

  return <div className={style.container}>
    <Paper elevation={3}>
      <DialogContent>
        <Formik
          initialValues={{
            password: '',
          }}
          validationSchema={changePasswordSchema}
          onSubmit={({password}) => changePassword({password, token})}
        >
          {({values, errors, touched, handleChange, handleBlur}) => (
            <Form>
              <TextField
                fullWidth
                name='password'
                type='password'
                label='New password'
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.password}
                error={!!(errors.password && touched.password)}
                helperText={touched.password && errors.password}
              />
              <DialogActions>
                <Button variant='contained' color="primary" type='submit'>
                  Change password
                </Button>
              </DialogActions>
            </Form>
          )}
        </Formik>
      </DialogContent>
    </Paper>
  </div>
}
