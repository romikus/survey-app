import React from 'react'
import TextField from '@material-ui/core/TextField/TextField'
import DialogActions from '@material-ui/core/DialogActions/DialogActions'
import Button from '@material-ui/core/Button'
import { Formik, Form } from 'formik'
import * as Yup from 'yup'
import {forgotPassword} from './actions'
import style from './style.module.css'

const Schema = Yup.object().shape({
  email: Yup.string().email('Invalid email').required(),
})

const instructions = "You will receive an email with instructions on how to reset your password in a few minutes."

export default () => {
  const [successMessage, setSuccessMessage] = React.useState<string>()

  return <Formik
    initialValues={{
      email: '',
    }}
    validationSchema={Schema}
    onSubmit={async (values, actions) => {
      console.log(values)
      try {
        await forgotPassword(values)
        setSuccessMessage(instructions)
      } catch (err) {
        if (err.type === 'not_found')
          actions.setFieldError('email', 'Email is not registered')
        else
          throw err
      }
    }}
  >
    {({values, errors, touched, handleChange, handleBlur}) => (
      <Form>
        {successMessage && <div className={style.notice}>{successMessage}</div>}
        <TextField
          fullWidth
          name='email'
          type='email'
          label='Email'
          onChange={handleChange}
          onBlur={handleBlur}
          value={values.email}
          error={!!(errors.email && touched.email)}
          helperText={touched.email && errors.email}
        />
        <DialogActions>
          <Button variant='contained' color="primary" type='submit'>
            Send confirmation email
          </Button>
        </DialogActions>
      </Form>
    )}
  </Formik>
}
