import React from 'react'
import {RouteComponentProps} from 'react-router'
import {verify} from './actions'

export default ({location: {search}}: RouteComponentProps) => {
  const [valid, setValid] = React.useState(true)

  React.useEffect(() => {
    const match = search.match(/\btoken=([^&]+)/)
    if (!match)
      return

    verify({token: match[1]}).then(setValid)
  }, [])

  if (valid)
    return null
  else
    return <div style={{minHeight: '100%', alignItems: 'center', justifyContent: 'center', display: 'flex'}}>
      Invalid verification link
    </div>
}
