import {post} from 'utils/fetch'
import {routes} from 'config/routes'
import {setCurrentUser} from 'components/User/actions'

export const verify = async ({token}: {token: string}) => {
  const data = await post(routes.api.user.verify(), {token})
  if (data.user) {
    const {user, accessToken} = data
    setCurrentUser({user, accessToken})
    return true
  } else {
    return false
  }
}
