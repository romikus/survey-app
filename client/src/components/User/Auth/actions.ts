import {routes} from 'config/routes'
import {post} from 'utils/fetch'
import {setCurrentUser} from 'components/User/actions'

type emailAndPassword = {
  email: string,
  password: string
}

export const signIn = async ({email, password}: emailAndPassword) => {
  const {user, accessToken} = await post(routes.api.user.signIn(), {email, password})
  setCurrentUser({user, accessToken})
}

export const signUp = async ({email, password}: emailAndPassword) => {
  await post(routes.api.user.signUp(), {email, password})
}

export const forgotPassword = async ({email}: {email: string}) => {
  await post(routes.api.user.forgotPassword(), {email})
}
