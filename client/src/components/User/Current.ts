import {action, observable} from 'mobx'
import UserType from './Type'

class CurrentUser {
  @observable loaded: boolean = false
  @observable user: UserType | undefined = undefined

  @action setUser(user?: UserType) {
    this.loaded = true
    this.user = user
  }
}

export default new CurrentUser()
