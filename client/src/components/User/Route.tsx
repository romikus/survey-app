import React from 'react'
import {Route, Redirect} from 'react-router-dom';
import {observer} from 'mobx-react'
import {getCurrentUser} from 'components/User/actions'
import CurrentUser from 'components/User/Current'
import {routes} from 'config/routes'

type AuthUserProps = {authorized: boolean, Component: React.ElementType}
const AuthUserRoute = observer(({authorized, Component, ...props}: AuthUserProps) => {
  React.useEffect(() => {
    getCurrentUser()
  }, [])

  if (!CurrentUser.loaded)
    return null

  if (authorized ? CurrentUser.user : !CurrentUser.user)
    return <Component {...props} />
  else
    return <Redirect to={authorized ? routes.user.signIn() : routes.root()} />
})

type props = {path: string, component: React.ElementType, exact?: boolean}

export const UserRoute = ({path, exact, component}: props) =>
  <Route path={path} exact={exact} render={(props) =>
    <AuthUserRoute authorized Component={component} {...props} />
  }/>

export const NoUserRoute = ({path, exact, component}: props) =>
  <Route path={path} exact={exact} render={(props) =>
    <AuthUserRoute authorized={false} Component={component} {...props} />
  }/>
